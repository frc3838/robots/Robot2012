package frc3838.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc3838.robot.commands.BridgeDownCommand;
import frc3838.robot.commands.BridgeUpCommand;
import frc3838.robot.commands.camera.ToggleLedOnOffCommand;
import frc3838.robot.commands.drive.ShiftDownCommand;
import frc3838.robot.commands.drive.ShiftUpCommand;
import frc3838.robot.commands.drive.TurnOffLimitedSpeedCommand;
import frc3838.robot.commands.drive.TurnOnLimitedSpeedCommand;
import frc3838.robot.commands.shooting.*;
import frc3838.robot.commands.shooting.calculations.CycleBasketHeightCommand;
import frc3838.robot.commands.shooting.calculations.DecrDCommand;
import frc3838.robot.commands.shooting.calculations.IncrDCommand;
import frc3838.robot.utils.LOG;


/** @noinspection FieldCanBeLocal*/
public class OI
{
    private Button cameraLedRingButton;

    private Button shiftDownButton;
    private Button shiftUpButton;
    private Button turnOnLimitedSpeedButton;
    private Button turnOffLimitedSpeedButton;
//    private Button togglingLimitedSpeedModeButton;

    private Button increaseThrowMotorSpeedButton;
    private Button decreaseThrowMotorSpeedButton;
    private Button changeThrowMotorSpeedButton;
    private Button startThrowMotorButton;
    private Button stopThrowMotorButton;


    private Button incrDButton;
    private Button decrDButton;
    private Button cycleBasketHeightButton;

    private Button fireButton;

    private Button bridgeDownButton;

    private static final Joystick gunnerJoystick = RobotMap.ControlStationMap.GUNNER_JOYSTICK;
    private static final Joystick driverJoystick = RobotMap.ControlStationMap.DRIVER_JOYSTICK;


    public OI()
    {
        LOG.debug("Entering OI Constructor");
        initCameraLedRingControls();
        initShiftControls();
        initShootingCalculationControls();
        initThrowMotorControlButtons();
        initSpeedLimitControls();
        initTargetingControls();
        initFiringPinControls();
        initBridgeControls();
        LOG.debug("Exiting OI Constructor");
    }


    private void initShootingCalculationControls()
    {
        try
        {
            if (Setup.isShootingCalculationsSubsystemEnabled())
            {
                LOG.trace("Initializing shooting calculations subsystem buttons");
                incrDButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.INCR_DIST_BTN_NUM);
                decrDButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.DECR_DIST_BTN_NUM);
                cycleBasketHeightButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.CYCLE_HEIGHT_BTN_NUM);

                incrDButton.whileHeld(new IncrDCommand());
                decrDButton.whileHeld(new DecrDCommand());
                cycleBasketHeightButton.whileHeld(new CycleBasketHeightCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the shooting calculation controls.", e);
        }
    }


    private void initShiftControls()
    {
        try
        {
            if (Setup.isDriveGearShiftingEnabled())
            {
                LOG.trace("Initializing shifter buttons");
                shiftDownButton = new JoystickButton(driverJoystick, RobotMap.ControlStationMap.DriverJoystickButtons.SHIFT_TO_LOW_GEAR_BTN_NUM);
                shiftUpButton = new JoystickButton(driverJoystick, RobotMap.ControlStationMap.DriverJoystickButtons.SHIFT_TO_HIGH_GEAR_BTN_NUM);

                shiftUpButton.whenPressed(new ShiftUpCommand());
                shiftDownButton.whenPressed(new ShiftDownCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the drive shift controls.", e);
        }
    }

    private void initSpeedLimitControls()
    {
        try
        {
            if (Setup.isDriveSpeedLimitEnabled())
            {
                LOG.trace("Initializing Limited Speed Mode Buttons");
//                togglingLimitedSpeedModeButton = new JoystickButton(RobotMap.ControlStationMap.DRIVER_JOYSTICK, RobotMap.ControlStationMap.DriverJoystickButtons.BTN_TOGGLING_LIMITED_SPEED_MODE);
//                togglingLimitedSpeedModeButton.whenPressed(new ToggleLimitedSpeedModeCommand());

                turnOffLimitedSpeedButton = new JoystickButton(driverJoystick, RobotMap.ControlStationMap.DriverJoystickButtons.LIMITED_SPEED_OFF_BTN_NUM);
                turnOffLimitedSpeedButton.whenPressed(new TurnOffLimitedSpeedCommand());

                turnOnLimitedSpeedButton = new JoystickButton(driverJoystick, RobotMap.ControlStationMap.DriverJoystickButtons.LIMITED_SPEED_ON_BTN_NUM);
                turnOnLimitedSpeedButton.whenPressed(new TurnOnLimitedSpeedCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the speed limit controls.", e);
        }
    }

    private void initCameraLedRingControls()
    {
        try
        {
            if (Setup.isCameraLedRingControlEnabled())
            {
                LOG.trace("Initializing LED ring button");
                cameraLedRingButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.LED_ON_OFF_BTN_NUM);

                cameraLedRingButton.whenPressed(new ToggleLedOnOffCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the camera led ring controls.", e);
        }
    }

    private void initThrowMotorControlButtons()
    {
        try
        {
            if (Setup.isThrowMotorEnabled())
            {
                LOG.trace("Initializing throw motor control buttons");
                changeThrowMotorSpeedButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.THROW_MOTOR_SPEED_CHANGE_SHIFT_BTN_NUM);
                changeThrowMotorSpeedButton.whileHeld(new ControlThrowSpeedCommand());


                startThrowMotorButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.THROW_MOTOR_START_BTN_NUM);
                startThrowMotorButton.whenPressed(new StartThrowMotorCommand());

                stopThrowMotorButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.THROW_MOTOR_STOP_BTN_NUM);
                stopThrowMotorButton.whenPressed(new StopThrowMotorCommand());
                
                increaseThrowMotorSpeedButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.INCREASE_THROW_MOTOR_SPEED_BTN_NUM);
                decreaseThrowMotorSpeedButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.DECREASE_THROW_MOTOR_SPEED_BTN_NUM);
//
                increaseThrowMotorSpeedButton.whenPressed(new IncreaseThrowMotorSpeedCommand());
                decreaseThrowMotorSpeedButton.whenPressed(new DecreaseThrowMotorSpeedCommand());
//
//                JoystickComboButton comboButton =
//                        new JoystickComboButton(gunnerJoystick,
//                        RobotMap.ControlStationMap.GunnerJoystickButtons.INCREASE_THROW_MOTOR_SPEED_BTN_NUM,
//                        RobotMap.ControlStationMap.GunnerJoystickButtons.DECREASE_THROW_MOTOR_SPEED_BTN_NUM);
//
//                comboButton.whenPressedButtonA(new IncreaseThrowMotorSpeedCommand());
//                comboButton.whenPressedButtonB(new DecreaseThrowMotorSpeedCommand());
//                comboButton.whenPressedCombined(new StopThrowMotorCommand());
//                comboButton.whenReleasedCombined(new StopThrowMotorCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when OI.initThrowMotorControlButtons was executed.", e);
        }
    }

    private void initTargetingControls()
    {
        if (Setup.isTargetingSubsystemEnabled())
        {

        }
    }

    private void initFiringPinControls()
    {
        try
        {
            if (Setup.isFiringPinSubsystemEnabled())
            {
                LOG.trace("Initializing Fire and Shoot Button");
                fireButton = new JoystickButton(gunnerJoystick, RobotMap.ControlStationMap.GunnerJoystickButtons.TARGET_AND_SHOOT_BTN_NUM);
                fireButton.whenPressed(new FiringPinShootCommand());
                fireButton.whenReleased(new FiringPinLoadCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when OI.initFiringPinControls was executed.", e);
        }
    }

    private void initBridgeControls()
    {
        try
        {
            if (Setup.isBridgeArmSubsystemEnabled())
            {
                LOG.trace("Initializing Bridge Arm Subsystem");
                bridgeDownButton = new JoystickButton(driverJoystick, RobotMap.ControlStationMap.DriverJoystickButtons.BRIDGE_DOWN_BTN_NUM);
                bridgeDownButton.whenPressed(new BridgeDownCommand());
                bridgeDownButton.whenReleased(new BridgeUpCommand());
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when OI.initFiringPinControls was executed.", e);
        }
    }
}


//// CREATING BUTTONS
// One type of button is a joystick button which is any button on a joystick.
// You create one by telling it which joystick it's on and which button
// number it is.
// Joystick stick = new Joystick(port);
// Button button = new JoystickButton(stick, buttonNumber);

// Another type of button you can create is a DigitalIOButton, which is
// a button or switch hooked up to the cypress module. These are useful if
// you want to build a customized operator interface.
// Button button = new DigitalIOButton(1);

// There are a few additional built in buttons you can use. Additionally,
// by subclassing Button you can create custom triggers and bind those to
// commands the same as any other Button.

//// TRIGGERING COMMANDS WITH BUTTONS
// Once you have a button, it's trivial to bind it to a button in one of
// three ways:

// Start the command when the button is pressed and let it run the command
// until it is finished as determined by it's isFinished method.
// button.whenPressed(new ExampleCommand());

// Run the command while the button is being held down and interrupt it once
// the button is released.
// button.whileHeld(new ExampleCommand());