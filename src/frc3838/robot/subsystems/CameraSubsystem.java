/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.utils.LOG;


/**
 * @noinspection PointlessBooleanExpression, ConstantConditions, UnusedDeclaration
 */
public class CameraSubsystem extends Subsystem
{
    private static Relay ledRingRelay;
    private static boolean ledRingIsOn;


    //static initializer block
    static
    {
        LOG.trace("CameraControlSubsystem initializer block executing");
        initLedRing();
        LOG.debug("CameraControlSubsystem initializer block exiting");
    }


    private static void initLedRing()
    {
        try
        {
            if (Setup.isCameraLedRingControlEnabled() && ledRingRelay == null)
            {
                LOG.trace("initializing Led Ring Relay");
                ledRingRelay = new Relay(RobotMap.Relays.LED_RING);
                ledRingRelay.setDirection(Relay.Direction.kForward);
                turnLedRingOffImpl();
            }
            else
            {
                LOG.info("CameraLedRingControl is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when CameraSubsystem.initLedRing was executed.", e);
        }
    }


    private static void turnLedRingOffImpl()
    {
        try
        {
            if (Setup.isCameraLedRingControlEnabled())
            {
                LOG.trace("CameraSubsystem.turnLedRingOffImpl() called");
                ledRingRelay.set(Relay.Value.kOff);
                ledRingIsOn = false;
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when CameraSubsystem.turnLedRingOffImpl was executed.", e);
        }
    }

    public void turnLedRingOff()
    {
        turnLedRingOffImpl();
    }

    private static void turnLedRingOnImpl()
    {
        try
        {
            if (Setup.isCameraLedRingControlEnabled())
            {
                LOG.trace("CameraSubsystem.turnLedRingOnImpl() called");
                ledRingRelay.set(Relay.Value.kOn);
                ledRingIsOn = true;
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when CameraSubsystem.turnLedRingOnImpl was executed.", e);
        }
    }

    public void turnLedRingOn()
    {
        turnLedRingOnImpl();
    }

    /**
     * Toggles camera LED_RING ring on and off.
     *
     * @return true if LED_RING was turned on; false is LED_RING was turned off.
     */
    public boolean toggleLedRing()
    {
        try
        {
            LOG.trace("CameraSubsystem.toggleLedRing() called. Starting value of ledRingIsOn=" + ledRingIsOn);
            if (ledRingIsOn)
            {
                turnLedRingOffImpl();
            }
            else
            {
                turnLedRingOnImpl();
            }
            LOG.trace("CameraSubsystem.toggleLedRing() exiting. Ending value of ledRingIsOn=" + ledRingIsOn);
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when CameraSubsystem.toggleLedRing was executed.", e);
        }
        return ledRingIsOn;
    }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
