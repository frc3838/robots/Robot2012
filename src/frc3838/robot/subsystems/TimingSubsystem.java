/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.robot.commands.PingTimerCommand;
import frc3838.robot.utils.LOG;



public class TimingSubsystem extends Subsystem
{
    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        LOG.trace("initializing default command to PingTimerCommand");
        setDefaultCommand(new PingTimerCommand());
        LOG.trace("initialization of default command completed");
    }
}
