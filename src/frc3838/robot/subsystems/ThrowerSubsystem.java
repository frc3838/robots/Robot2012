/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.Counter;
import edu.wpi.first.wpilibj.DriverStationLCD;
import edu.wpi.first.wpilibj.PWM;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.commands.shooting.calculations.MonitorThrowMotor;
import frc3838.robot.utils.LOG;
import frc3838.robot.utils.math.MathAids;



public class ThrowerSubsystem extends Subsystem
{
    public static final boolean DEBUG_THROW_MOTOR = false;
    /**
     * Flag to determine is monitoring thread should keep executing to report the status of the throw motor.
     */


    private static boolean reportMonitoring = true;
    private static PWM throwMotor;
    private static Counter throwMotorEncoder;

    /**
     * @noinspection FieldCanBeLocal
     */
    private static final double ENCODER_SCALE = 60 / 32;

    /**
     * @noinspection FieldCanBeLocal
     */
//    private static Thread monitorThread;


    /*
       From PDW documentation...
       As of revision 0.1.4 of the FPGA, the FPGA interprets the 0-255 values as follows:
         255 = full "forward"
         254 to 129 = linear scaling from "full forward" to "center"
         128 = center value
         127 to 2 = linear scaling from "center" to "full reverse"
         1 = full "reverse"
         0 = disabled (i.e. PWM output is held low)
    */

    private static final int RAW_VALUE_DISABLED = 0;
    private static final int RAW_VALUE_STOP = 128;
    private static final int RAW_VALUE_MIN = 0;
    public static final int RAW_VALUE_MIN_FORWARD = RAW_VALUE_STOP + 25;
    private static final int RAW_VALUE_MAX_REVERSE = RAW_VALUE_STOP - 1;
    public static final int RAW_VALUE_MAX = 255;
    private static final int MAX_RELATIVE_SPEED = RAW_VALUE_MAX - RAW_VALUE_STOP;
    private static int rawValueSetting = RAW_VALUE_MIN_FORWARD;
    private static double currentRPM = 0.0;
    private static String speedPercentage = "0%";
    private static MotorState motorState = MotorState.Stopped;

    private static DriverStationLCD lcd = DriverStationLCD.getInstance();
    private static final DriverStationLCD.Line lcdLine = RobotMap.ControlStationMap.SHOOTER_STATUS_LCD_LINE;


    //static initializer block
    static
    {
        LOG.trace("ThrowerSubsystem initializer block executing");
        initThrowMotor();
        LOG.debug("ThrowerSubsystem initializer block exiting");
    }

    private static void initThrowMotor()
    {
        try
        {
            if (Setup.isThrowMotorEnabled() && throwMotor == null)
            {
                LOG.trace("initializing throw motor");
                throwMotor = new PWM(RobotMap.PWMChannels.THROW_MOTOR_CHANNEL);

                throwMotorEncoder = new Counter(RobotMap.DigitalIO.THROW_MOTOR_ENCODER);
                throwMotorEncoder.reset();
                throwMotorEncoder.start();
                // Modify a counter to change the getPID() method to return
                // 1/Period?  Then use this in a PIDController Loop.

//                startMonitoringImpl();

            }
            else
            {
                LOG.info("ThrowMotor is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ThrowerSubsystem.initThrowMotor was executed.", e);
        }
    }


//    private static void startMonitoringImpl()
//    {
//        reportMonitoring = true;
//        monitorThread = new Thread()
//        {
//            public void run()
//            {
//
//                while (reportMonitoring)
//                {
//                    updateSmartDashboardImpl();
//                    try { Thread.sleep(250); } catch (InterruptedException ignore) { }
//                }
//            }
//
//
//
//        };
//        monitorThread.start();
//    }
//
//    public void startMonitoringThrowMotor()
//    {
//        //if we are not already running a monitoring thread, start one.
//        if (!reportMonitoring)
//        {
//            startMonitoringImpl();
//        }
//    }
//
//    public void stopMonitoringThrowMotor()
//    {
//        reportMonitoring = false;
//    }


    public void increaseThrowMotorSpeedSetting(int delta)
    {
        if (DEBUG_THROW_MOTOR)
        {
            LOG.trace("increaseThrowMotorSpeedSetting() called with delta of " + delta + ". rawValueSetting before change: " + rawValueSetting);
        }
        rawValueSetting += delta;
        setThrowMotorForwardSpeed();
    }

    public void decreaseThrowMotorSpeedSetting(int delta)
    {
        if (DEBUG_THROW_MOTOR)
        {
            LOG.trace("decreaseThrowMotorSpeedSetting() called with delta of " + delta + ". rawValueSetting before change: " + rawValueSetting);
        }
        rawValueSetting -= delta;
        setThrowMotorForwardSpeed();
    }


    /**
     * Sets the throw motor's raw speed. The speed must be between
     * the {@link #RAW_VALUE_MIN_FORWARD} speed and the {@link #RAW_VALUE_MAX}.
     * Any value outside this range will be adjusted to within the range.
     * <b>Not that this method only sets the motor's speed. It does not start it
     * if it is not running,</b>
     * @param rawSpeed the speed to set
     */
    public void setThrowMotorSpeed(int rawSpeed)
    {
        rawValueSetting = rawSpeed;
        setThrowMotorForwardSpeed();
    }


    public void startThrowMotor()
    {
        if (DEBUG_THROW_MOTOR)
        {
            LOG.trace("startThrowMotor() called when rawValueSetting = " + rawValueSetting);
        }

        if (rawValueSetting < RAW_VALUE_MIN_FORWARD)
        {
            if (DEBUG_THROW_MOTOR)
            {
                LOG.debug("rawValueSetting is less than RAW_VALUE_MIN_FORWARD of " + RAW_VALUE_MIN_FORWARD + ". Setting rawValueSetting to RAW_VALUE_MIN_FORWARD");
            }
            rawValueSetting = RAW_VALUE_MIN_FORWARD;
        }

        setThrowMotorRawValue(rawValueSetting);
    }

    public void stopThrowMotor()
    {
        if (DEBUG_THROW_MOTOR)
        {
            LOG.trace("stopThrowMotor() called. Setting raw value to " + RAW_VALUE_STOP);
        }
        setThrowMotorRawValue(RAW_VALUE_STOP);
    }

    private static void setThrowMotorForwardSpeed()
    {
        if (DEBUG_THROW_MOTOR)
        {
            LOG.trace("setThrowMotorForwardSpeed() called while rawValueSetting = " + rawValueSetting);
        }


        if (rawValueSetting > RAW_VALUE_MAX)
        {
            if (DEBUG_THROW_MOTOR)
            {
                LOG.debug("setThrowMotorForwardSpeed(): rawValueSetting " + rawValueSetting + " exceeds RAW_VALUE_MAX of " + RAW_VALUE_MAX + ". Setting to RAW_VALUE_MAX");
            }
            rawValueSetting = RAW_VALUE_MAX;
        }

        if (rawValueSetting < RAW_VALUE_MIN_FORWARD)
        {
            if (DEBUG_THROW_MOTOR)
            {
                LOG.debug("setThrowMotorForwardSpeed(): rawValueSetting " + rawValueSetting + " below RAW_VALUE_MIN_FORWARD of " + RAW_VALUE_MIN_FORWARD + ". Setting to RAW_VALUE_MIN_FORWARD");
            }
            rawValueSetting = RAW_VALUE_MIN_FORWARD;
        }

        // if we are running (in a forward direction), we want to set the value on the motor. If we are not running, we do
        // not want to set it on the motor we just hold it for later use when the motor is started so  that it starts at that speed.
        if (throwMotorIsRunningForward())
        {
            if (DEBUG_THROW_MOTOR)
            {
                LOG.trace("Throw Motor is running. Calling setThrowMotorRawValue for value of " + rawValueSetting);
            }
            setThrowMotorRawValue(rawValueSetting);
        }
        else
        {
            if (DEBUG_THROW_MOTOR)
            {
                LOG.trace("Changing throw motor relative speed to " + rawValueSetting + " for when motor is started next.");
            }
        }
        updateSmartDashboardImpl();
    }

    private static void setThrowMotorRawValue(int rawValue)
    {
        if (DEBUG_THROW_MOTOR)
        {
            LOG.trace("setThrowMotorRawValue called with a value of " + rawValue);
        }

        if (rawValue < RAW_VALUE_STOP)
        {
            if (DEBUG_THROW_MOTOR)
            {
                LOG.debug("setThrowMotorRawValue(): rawValue + " + rawValue + " is below RAW_VALUE_STOP of " + RAW_VALUE_STOP + ". Setting to RAW_VALUE_STOP");
            }
            rawValue = RAW_VALUE_STOP;
        }
        else if (rawValue > RAW_VALUE_MAX)
        {
            if (DEBUG_THROW_MOTOR)
            {
                LOG.debug("setThrowMotorRawValue(): rawValue + " + rawValue + " is exceeds RAW_VALUE_MAX of " + RAW_VALUE_MAX + ". Setting to RAW_VALUE_MAX");
            }
            rawValue = RAW_VALUE_MAX;
        }

        if (DEBUG_THROW_MOTOR)
        {
            LOG.debug("setThrowMotorRawValue(): >>> Setting throw motor raw value to: " + rawValue);
        }

        throwMotor.setRaw(rawValue);

        calcSpeedPercentage();
        calcMotorDirection();

        updateSmartDashboardImpl();
        updateDriverStationLCDImpl();
    }


    public static boolean throwMotorIsRunningForward()
    {
        return throwMotor.getRaw() > RAW_VALUE_STOP;
    }


    public static boolean throwMotorIsRunningReverse()
    {
        return throwMotor.getRaw() < RAW_VALUE_STOP;
    }


    /**
     * Checks if the throw motor is set to a stopped value. This does not indicate the motor is actually at a stop since it may still be spinning down from when
     * it was set to stop.
     *
     * @return if the throw motor is set to a stopped value
     */
    public static boolean throwMotorIsSetToStopped()
    {
        return throwMotor.getRaw() == RAW_VALUE_STOP || throwMotor.getRaw() == RAW_VALUE_DISABLED;
    }


    private static void calcSpeedPercentage() {speedPercentage =  MathAids.toPercentage((rawValueSetting - RAW_VALUE_STOP) / MAX_RELATIVE_SPEED);}


    public int getCount()
    {
        return getCountImpl();
    }


    private static int getCountImpl()
    {
        return throwMotorEncoder.get();
    }


    public double getRPM()
    {
        return currentRPM;
    }


    private static void calcAndSetRPM() // Returns speed in RPM
    {
        double speed = throwMotorEncoder.getPeriod();
        if (speed == 0)
        {
            currentRPM = 0.0;
        }
        else
        {
            currentRPM = ENCODER_SCALE / speed; // speed in RPM.  Note 32 Counts/Rev.
        }
    }


    private void updateDriverStationLCD()
    {
        updateDriverStationLCDImpl();
    }

    private static void updateDriverStationLCDImpl()
    {
        //Max line length is defined via DriverStationLCD.kLineLength, which is currently 21

        String status;
        if (throwMotorIsSetToStopped())
        {
            status = "STOPPED";
        }
        else
        {
            status = speedPercentage;
        }
        StringBuffer sb = new StringBuffer("Shooter: ").append(status);
        lcd.println(lcdLine, 1, sb);

    }

    public void  updateSmartDashboard()
    {
        updateSmartDashboardImpl();
    }

    private static void updateSmartDashboardImpl()
    {

        SmartDashboard.putString("Throw Motor State", getMotorStateImpl().toString());

        String displayValue = new StringBuffer(speedPercentage).append('(').append(rawValueSetting).append(')').toString();
        SmartDashboard.putString("Throw Motor Raw Setting", displayValue);

        if (DEBUG_THROW_MOTOR)
        {
            SmartDashboard.putInt("Throw Motor Actual Raw Value", throwMotor.getRaw());
            calcAndSetRPM();
            SmartDashboard.putDouble("Throw Motor RPM", currentRPM);
            SmartDashboard.putInt("Throw Motor Count", getCountImpl());
        }
    }

    private static MotorState getMotorStateImpl()
    {

        return motorState;
    }

    private static void calcMotorDirection()
    {
        if (throwMotorIsRunningForward())
        {
            motorState =  MotorState.Forward;
        }
        else if (throwMotorIsSetToStopped())
        {
            motorState = MotorState.Stopped;
        }
        else if (throwMotorIsRunningReverse())
        {
            motorState = MotorState.Reverse;
        }
    }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
         setDefaultCommand(new MonitorThrowMotor());
    }
}
