
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.Accelerometer;
import edu.wpi.first.wpilibj.AnalogChannel;
import edu.wpi.first.wpilibj.Gyro;
import edu.wpi.first.wpilibj.HiTechnicCompass;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.commands.RecordSensorReadingsCommand;
import frc3838.robot.utils.LOG;
import frc3838.robot.utils.math.StatsCalculator;

/**
 *
 * @author student18
 */
public class SenorsSubsystem extends Subsystem
{

    private static Accelerometer accelerometer;
    private static HiTechnicCompass compass;
    private static Gyro gyro;
    private static AnalogChannel ultrasonicAnalogChannel;
    public static final int NOT_INITIALIZED_FLAG_VALUE = -9999;
    private StatsCalculator compassCalc = new StatsCalculator(30);
    private StatsCalculator accelerationCalc = new StatsCalculator(30);
    private StatsCalculator gyroCalc = new StatsCalculator(30);
    private StatsCalculator distanceCalc = new StatsCalculator(30);

    //static initializer block
    static
    {
        LOG.debug("Initializing sensors");
        initAccelerometer();
        initCompass();
        initGyro();
        initUltrasonicSensor();
        LOG.debug("sensors initialization completed");
    }


    private static void initUltrasonicSensor()
    {
        try
        {
            if (Setup.isSensorUltrasoundEnabled())
            {
                LOG.trace("Constructing ultrasonicAnalogChannel");
                ultrasonicAnalogChannel = new AnalogChannel(RobotMap.AnalogIO.PROXIMITY_CHANNEL);
            }
            else
            {
                LOG.info("Ultrasound is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when SenorsSubsystem.initUltrasonicSensor was executed.", e);;
        }
    }


    private static void initGyro()
    {
        try
        {
            if (Setup.isSensorGyroEnabled())
            {
                LOG.trace("constructing gyro");
                AnalogChannel gyroAnalogChannel = new AnalogChannel(RobotMap.AnalogIO.GYRO_RATE_ADDR);
                gyro = new Gyro(gyroAnalogChannel);
                //Per documentation, sensitivity is 7mC/degree/s
                gyro.setSensitivity(0.007);
                gyro.reset();
            }
            else
            {
                LOG.info("Gyro is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when SenorsSubsystem.initGyro was executed.", e);
        }
    }


    private static void initCompass()
    {
        try
        {
            if (Setup.isSensorCompassEnabled())
            {
                LOG.trace("constructing compass");
                compass = new HiTechnicCompass(RobotMap.I2CBus.COMPASS_SLOT);
            }
            else
            {
                LOG.info("Compass is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when SenorsSubsystem.initCompass was executed.", e);
        }
    }


    private static void initAccelerometer()
    {
        try
        {
            if (Setup.isSensorAccelerometerEnabled())
            {
                LOG.trace("constructing accelerometer");
                accelerometer = new Accelerometer(RobotMap.DigitalIO.ACCELEROMETER_ADDR);
            }
            else
            {
                LOG.info("Accelerometer is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when SenorsSubsystem.initAccelerometer was executed.", e);
        }
    }


    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    public double getAcceleration()
    {
        if (accelerometer != null)
        {
            try
            {
                double acceleration = accelerometer.getAcceleration();
                accelerationCalc.addValue(acceleration);
                return acceleration;
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred when SenorsSubsystem.getAcceleration was executed.", e);
                return NOT_INITIALIZED_FLAG_VALUE;
            }
        }
        else
        {
            return NOT_INITIALIZED_FLAG_VALUE;
        }
    }

    public double getAccelerationSD()
    {
        return accelerationCalc.calcStandardDeviation();
    }

    public double getCompassAngle()
    {
        if (compass != null)
        {
            try
            {
                double compassAngle = compass.getAngle();

                compassCalc.addValue(compassAngle);
                return compassAngle;
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred when SenorsSubsystem.getCompassAngle was executed.", e);
                return NOT_INITIALIZED_FLAG_VALUE;
            }
        }
        else
        {
            return NOT_INITIALIZED_FLAG_VALUE;
        }
    }

    public double getCompassAngleSD()
    {
        return compassCalc.calcStandardDeviation();
    }

    public double getGyroAngle()
    {
        if (gyro != null)
        {
            try
            {
                double angle = gyro.getAngle();
                gyroCalc.addValue(angle);
                return angle;
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred when SenorsSubsystem.getGyroAngle was executed.", e);
                return NOT_INITIALIZED_FLAG_VALUE;
            }
        }
        else
        {
            return NOT_INITIALIZED_FLAG_VALUE;
        }
    }

    public double getGyroAngleSD()
    {
        try
        {
            return gyroCalc.calcStandardDeviation();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when SenorsSubsystem.getGyroAngleSD was executed.", e);
            return NOT_INITIALIZED_FLAG_VALUE;
        }
    }

    public double getGyroPidGet()
    {
        if (gyro != null)
        {
            try
            {
                return gyro.pidGet();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred when SenorsSubsystem.getGyroPidGet was executed.", e);
                return NOT_INITIALIZED_FLAG_VALUE;
            }
        }
        else
        {
            return NOT_INITIALIZED_FLAG_VALUE;
        }
    }

    public double getDistance()
    {
        if (ultrasonicAnalogChannel != null)
        {
            try
            {
                double distance = ultrasonicAnalogChannel.getValue();
                distanceCalc.addValue(distance);
                return distance;
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred when SenorsSubsystem.getDistance was executed.", e);
                return NOT_INITIALIZED_FLAG_VALUE;
            }
        }
        else
        {
            return NOT_INITIALIZED_FLAG_VALUE;
        }

    }

    public double getDistanceSD()
    {
        return distanceCalc.calcStandardDeviation();
    }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        // setDefaultCommand(new MySpecialCommand());
        LOG.trace("initializing default command to RecordSensorReadingsCommand");
        setDefaultCommand(new RecordSensorReadingsCommand());
        LOG.trace("initialization of default command completed");
    }
}
