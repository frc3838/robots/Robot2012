/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/
package frc3838.robot;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import frc3838.robot.commands.AutonomousCommandGroup;
import frc3838.robot.commands.CommandBase;
import frc3838.robot.utils.LOG;



/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 * @noinspection RefusedBequest
 */
public class RocCityRobot extends IterativeRobot
{

    Command autonomousCommand;

    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    public void robotInit()
    {
        LOG.debug("entering robotInit method");

        // Note: the original template has the call to CommandBase.init() coming after creating the
        //        autonomousCommand and any teleop commands. But there is the possibility that a command
        //        requires a subsystem. In that case, the subsystem would be null. So it would seem that
        //        the call to init() must come first. It shouldn't break anything coming first.
        // Initialize all subsystems


        CommandBase.init();

        // instantiate the command used for the autonomous period
        autonomousCommand = new AutonomousCommandGroup();

        // instantiate commands used for the teleop period


        LOG.debug("exit robotInit method");
    }


    public void autonomousInit()
    {
        // schedule the autonomous command (example)
        autonomousCommand.start();
    }

    /**
     * This function is called periodically during autonomous
     */
    public void autonomousPeriodic()
    {
        Scheduler.getInstance().run();
    }

    public void teleopInit()
    {
        // This makes sure that the autonomous stops running when
        // teleop starts running. If you want the autonomous to
        // continue until interrupted by another command, remove
        // this line or comment it out.
        autonomousCommand.cancel();
        RobotMap.inAutonomousMode = false;
    }

    /**
     * This function is called periodically during operator control
     */
    public void teleopPeriodic()
    {
        Scheduler.getInstance().run();
    }

}
