package frc3838.robot.utils;



import frc3838.robot.RobotMap;


/**
 * A class to simplify logging messages to the console. This class logs messages to
 * the System output console. Logging can be made at the following levels, as defined
 * in the {@link LogLevel} class. Levels are listed from the least verbose, to the
 * most verbose.
 * <ol>
 *     <li>{@link LogLevel#OFF OFF}: no logging occurs</li>
 *     <li>{@link LogLevel#ERROR ERROR}: Only error messages are logged</li>
 *     <li>{@link LogLevel#INFO INFO}: Only error and information messages are logged</li>
 *     <li>{@link LogLevel#DEBUG DEBUG}: A more verbose level in which debugging information, along with error and information messages are logged</li>
 *     <li>{@link LogLevel#TRACE TRACE}: The most verbose level in which all messages are logged including the highly verbose trace level messages</li>
 * </ol>
 * Please see the {@link LogLevel} class documentation, and the individual logging methods of this class, for guidance on
 * what types of messages are appropriate for each level.
 * <p/>
 * <b>For those unfamiliar with typical logging frameworks, here is a quick primer.</b>
 * While programming, you determine what level is appropriate for a message.
 * <p/>
 * <b>TRACE</b> messages are very
 * verbose messages used to help trace the execution of a program. They are the good old fashion
 * &quot;Got Here&quot;, &quot;Entering method&quot;, and similar messages. They can also be used
 * to trace the setting and changing of variables
 * at a fine level. <br/>
 * <b>DEBUG</b> messages are messages that are useful in debugging a program.
 * Typically they are used to log the setting or changing of variables.<br/>
 * <b>INFO</b> messages log basic information of the program running. Typically major
 * milestones in the programs execution (&quot;initialization complete&quot;,
 * &quot;starting teleop mode&quot;;for example). Messages logged at the INFO level should be limited
 * to prevent overly verbose logging during normal operations.<br/>
 * <b>ERROR</b> messages are for logging error conditions or abnormal program execution.
 * <p/>
 * When you compile the code before a run, you can set the logging level
 * via the {@link RobotMap#LOG_LEVEL} constant. This will set the maximum level of verbosity
 * of messages that will be logged when the program is run. For example, if this level is set
 * to INFO, DEBUG and TRACE messages will <i>not</i> be logged. Only INFO an ERROR messages.
 * In this way, it acts as a filter.
 *
 */
public class LOG
{

    /**
     * The current LogLevel to use. By default this value is the value of the {@link RobotMap#LOG_LEVEL} constant. However, in the event that the Robot is NOT
     * is debug mode, as determined by the {@link RobotMap#IN_DEBUG_MODE}, this level will not be settable to a level any more verbose than INFO. This is to 1)
     * eliminate the (minor) performance cost of logging during the completion; and 2) eliminate unnecessary verbose output during the competition.
     */
    private static LogLevel currentLevel;

    /**
     * A variable that is set in the static initializer block and is used to pad the logging
     * level information in the the output to allow for a consistent width of level information.
     */
    private static int maxLevelLength = 5;

    /**
     * Can be use to set a desired length of the location information. Any location information
     * strings (i.e. the simple class name, method name, and line number information) that is
     * less than this value will be padded with whitespace. This can aid in lining log messages
     * up in the console output for easier reading.
     */
    private static final int locationLength = 5; //Setting to 5 for now since location creation is not working


    //static use only
    private LOG() {}


    //static initializer block - For info on such, see http://docs.oracle.com/javase/tutorial/java/javaOO/initial.html
    static
    {
        currentLevel = RobotMap.LOG_LEVEL;
        //If we are not in DEBUG mode, we do not want
        //to allow for a level of DEBUG or TRACE
        //noinspection ConstantConditions, PointlessBooleanExpression
        if (!RobotMap.IN_DEBUG_MODE && currentLevel.ordinal >= LogLevel.DEBUG.ordinal)
        {
            currentLevel = LogLevel.INFO;
        }

        //Set the maximum length of the level
        LogLevel[] levels = LogLevel.asArray();
        for (int i = 0; i < levels.length; i++)
        {
            maxLevelLength = Math.max(maxLevelLength, levels[i].toString().length());

        }
    } //emd static initializer block


    public static void error(String message)
    {
        logImpl(LogLevel.ERROR, message, null);
    }


    public static void error(String message, Exception e)
    {
        String msg = message + ' ' + e.toString();
        logImpl(LogLevel.ERROR, msg, e);
        com.sun.squawk.debugger.Log.log(msg);

    }

    public static void info(String message)
    {
        logImpl(LogLevel.INFO, message, null);
    }


    public static void debug(String message)
    {
        logImpl(LogLevel.DEBUG, message, null);
    }


    public static void trace(String message)
    {
        logImpl(LogLevel.TRACE, message, null);
    }

    public static void log(LogLevel level, String message)
    {
        logImpl(level, message, null);
    }



    /** @noinspection UseOfSystemOutOrSystemErr*/
    private static void logImpl(LogLevel level, String message, Exception e)
    {
        if (level != null && !LogLevel.OFF.equals(level))
        {
            if (level.ordinal <= currentLevel.ordinal)
            {


                StringBuffer logMsg = new StringBuffer();
                logMsg.append('[');
                logMsg.append(level);

                for (int i = 0; i < (maxLevelLength - level.toString().length()); i++)
                {
                    logMsg.append(' ');
                }

                logMsg.append("] ");


                logMsg.append(createLocationInfo());

                if (LogLevel.ERROR.equals(level))
                {
                    logMsg.append(" ***ERROR*** ");
                }
                logMsg.append(message);

                if ( e!= null)
                {
                    logMsg.append(" :: ").append(e.toString());
                }

                System.out.println(logMsg.toString());

                if (e != null)
                {
                    System.out.flush();
                    e.printStackTrace();
                    System.err.flush();
                }
            }
        }
    }

    private static StringBuffer createLocationInfo()
    {

// Unfortunately the squawk VM Exception implementation does no have the getStackTrace() method, so we can not use that to determine the location as planned :(
//        Exception e = new Exception();
        //index 0 is this method; index 1 is the logImp; index 2 is the public logging method; index 3 is the caller of the logging method
//        StackTraceElement stackTraceElement = e.getStackTrace()[3];
//        String className = stackTraceElement.getClassName();
//        String simpleClassName = className.substring(className.lastIndexOf('.') + 1);
//        String methodName = stackTraceElement.getMethodName();
//        int lineNumber = stackTraceElement.getLineNumber();
        StringBuffer output = new StringBuffer();
//        output.append(simpleClassName).append('.')
//              .append(methodName)
//              .append("():")
//              .append(lineNumber);
        int length = output.length();
        for (int i = 0; i < (locationLength - length); i++)
        {
            output.append(' ');
        }

        output.append(" -- ");
        return output;

    }
}
