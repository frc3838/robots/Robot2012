package frc3838.robot.utils.math;

import com.sun.squawk.util.MathUtils;


/**
 * A Standard Deviation calculator utility that returns the Standard Deviation for the last <i>x</i> values added. If less than <i>x</i> values have been added,
 * the {@link #calcStandardDeviation()} method will return {@link Double#NaN} until at least the desired sample size of values have been added. As new values
 * are added, the oldest value will be dropped from the data set. <p />
 *
 * Note that regardless of the type (int, double, etc.) of the data that is added (whether individually or as an array), this calculator will store the data as
 * a double. This simplifies operation and ensures that no loss in magnitude or precision of the values occur when calculating the standard deviation. If you
 * call the {@link #getObservations} method, it will return an array of {@code double}s. If you need the data set as a different data type, you can use one of
 * the {@code getDataSetAsXXX} methods. In general you should use the method that either corresponds to the data type you used to populate the data set, or one
 * that would result in a widening primitive conversion of the data type(s) you used to populate the data set. If you use a {@code getDataSetAsXXX} method that
 * results in a narrowing casting, you may lose magnitude, precision, or both. For example, if the data set was populated with floating point @{code double}s,
 * and the {@code getObservationsAsIntegers} method is used to retrieve the data set, the resultant set of data will have a loss of precision since the
 * fractional portion will be truncated, and there is the potential loss of magnitude due to a 64-bit number being cast into a 32-bit data type. In other words,
 * normal Java casting rules apply. See <a href="http://java.sun.com/docs/books/jls/third_edition/html/conversions.html">Chapter 5 <i>Conversions and
 * Promotions</i></a> of <a href="http://java.sun.com/docs/books/jls/">The Java Language Specification</a> for more detail. Finally, it should be noted that use
 * of the {@code getDataSetAsXXX} methods have a performance cost.
 *
 * @noinspection UnusedDeclaration, ZeroLengthArrayAllocation, OverlyComplexMethod
 */
public class StatsCalculator
{
    public static final int INFINITE_WINDOW = -1;
    private int windowSize;
    private double[] data;
    private int count = 0;


    public StatsCalculator()
    {
        windowSize = INFINITE_WINDOW;
    }


    public StatsCalculator(double[] data)
    {
        windowSize = INFINITE_WINDOW;
        addValues(data);
    }


    /**
     * Constructs a new StatsCalculator with the window size.
     *
     * @param windowSize the number of data items to include when calculating the standard deviation; must be 2 or more
     *
     * @throws IllegalArgumentException if the supplied windowSize is not 2 or more
     */
    public StatsCalculator(int windowSize) throws IllegalArgumentException
    {
        setWindowSize(windowSize);

        this.windowSize = windowSize;
        initDataArray();
    }


    public StatsCalculator(double[] data, int windowSize)
    {
        setWindowSize(windowSize);
        initDataArray();
        addValues(data);
    }


    private void setWindowSize(int windowSize)
    {

        if (windowSize < 2 && windowSize != INFINITE_WINDOW)
        {
            throw new IllegalArgumentException("Sample Size must be 2 or more. '" + windowSize + "' is invalid");
        }
        this.windowSize = windowSize;
    }


    private void initDataArray()
    {
        if (infiniteWindow())
        {
            data = null;
        }
        else
        {
            data = new double[windowSize];
            for (int i = 0; i < data.length; i++)
            {
                data[i] = Double.NaN;
            }
        }
    }


    private boolean infiniteWindow() {return windowSize == INFINITE_WINDOW;}


    /**
     * Adds a {@code double} value to the data set.
     *
     * @param value the value to add.
     */
    public void addValue(double value)
    {
        if (infiniteWindow())
        {
            if (data != null)
            {
                int length = data.length;
                double[] expandedArray = new double[length + 1];
                System.arraycopy(data, 0, expandedArray, 0, length);
                expandedArray[length] = value;
                data = expandedArray;
            }
            else
            {
                data = new double[1];
                data[0] = value;
            }
        }
        else
        {
            if (count < windowSize)
            {
                //We do not have a full populated array yet
                data[count++] = value;
            }
            else
            {
                //shift the array by one
                System.arraycopy(data, 1, data, 0, data.length - 1);
                data[data.length - 1] = value;
                count++;
            }
        }

    }


    /**
     * Adds a {@code byte} value to the data set.
     *
     * @param value the value to add.
     */
    public void addValue(byte value)
    {
        addValue((double) value);
    }


    /**
     * Adds a {@code short} value to the data set.
     *
     * @param value the value to add.
     */
    public void addValue(short value)
    {
        addValue((double) value);
    }


    /**
     * Adds a {@code int} value to the data set.
     *
     * @param value the value to add.
     */
    public void addValue(int value)
    {
        addValue((double) value);
    }


    /**
     * Adds a {@code long} value to the data set.
     *
     * @param value the value to add.
     */
    public void addValue(long value)
    {
        addValue((double) value);
    }


    /**
     * Adds a {@code float} value to the data set.
     *
     * @param value the value to add.
     */
    public void addValue(float value)
    {
        addValue((double) value);
    }


    /**
     * Adds an array of {@code double} values to the data set.
     *
     * @param values the values to add.
     */
    public void addValues(double[] values)
    {
        if (values == null || values.length == 0) { return; }

        if (infiniteWindow())
        {
            if (data == null)
            {
                data = values;
            }
            else
            {
                int originalLength = data.length;
                expandDataArray(values.length);
                //now copy the added elements
                System.arraycopy(values, 0, data, originalLength, values.length);
            }
        }
        else
        {
            int numToAdd = values.length;
            if (numToAdd >= windowSize)
            {
                //The number of values is greater than the max number, so we just copy the last windowSize values into the data array
                System.arraycopy(values, (values.length - windowSize), data, 0, data.length);
            }
            else
            {
                //Determine how many empty spots there are in the array, but do not allow a negative number
                int emptyCells = Math.max(0, windowSize - count);

                if (numToAdd <= emptyCells)
                {
                    //we can just put the values into the array starting at the correct index
                    System.arraycopy(values, 0, data, count, values.length);
                }
                else
                {
                    //First we must shift the end values over in the data array
                    int shiftAmt = (emptyCells < numToAdd) ? (numToAdd - emptyCells) : numToAdd;
                    shiftDataArray(shiftAmt);
                    //now copy the new values onto the end
                    System.arraycopy(values, 0, data, (data.length - numToAdd), numToAdd);
                }
            }
            count += numToAdd;
        }
    }


    private void shiftDataArray(int numToBeAdded) {System.arraycopy(data, numToBeAdded, data, 0, (data.length - numToBeAdded));}


    /**
     * Expands the {@link #data} array by the {@code numberOfAdditionalValues}. If the {@code data} array is null, it creates a new data array sized to the
     * received {@code numberOfAdditionalValues}.
     *
     * @param numberOfAdditionalValues how much to increase the data array size by,
     */
    private void expandDataArray(int numberOfAdditionalValues)
    {
        if (data != null)
        {
            double[] expandedArray = new double[data.length + numberOfAdditionalValues];
            //copy original data to expanded array
            System.arraycopy(data, 0, expandedArray, 0, data.length);
            data = expandedArray;
        }
        else
        {
            data = new double[numberOfAdditionalValues];
        }
    }


    /**
     * Adds an array of {@code byte} values to the data set.
     *
     * @param values the values to add.
     */
    public void addValues(byte[] values)
    {
        if (values == null || values.length == 0) { return; }

        // Since they are different data types, we cannot use System.copyarray. Additionally, the squawk VM
        // has limited Array utils. So we will need to manually cast and copy each element
        if (infiniteWindow())
        {
            int originalLength = data != null ? data.length : 0;
            expandDataArray(values.length);
            for (int i = 0; i < values.length; i++)
            {
                //the originalLength acts as an offset
                data[originalLength + i] = (double) values[i];
            }
        }
        else
        {
            int numToAdd = values.length;
            if (numToAdd >= windowSize)
            {
                //The number of values is greater than the max number, so we just copy the last windowSize values into the data array
                int offset = values.length - windowSize;
                for (int i = 0; i < data.length; i++)
                {
                    data[i] = (double) values[offset + i];
                }
            }
            else
            {
                //Determine how many empty spots there are in the array, but do not allow a negative number
                int emptyCells = Math.max(0, windowSize - count);

                if (numToAdd <= emptyCells)
                {
                    //we can just put the values into the array starting at the correct index
                    int offset = count;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
                else
                {
                    //First we must shift the end values over in the data array
                    int shiftAmt = (emptyCells < numToAdd) ? (numToAdd - emptyCells) : numToAdd;
                    shiftDataArray(shiftAmt);
                    //now copy the new values onto the end
                    int offset = data.length - values.length;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
            }
            count += numToAdd;
        }
    }


    /**
     * Adds an array of {@code short} values to the data set.
     *
     * @param values the values to add.
     */
    public void addValues(short[] values)
    {
        if (values == null || values.length == 0) { return; }

        // Since they are different data types, we cannot use System.copyarray. Additionally, the squawk VM
        // has limited Array utils. So we will need to manually cast and copy each element
        if (infiniteWindow())
        {
            int originalLength = data != null ? data.length : 0;
            expandDataArray(values.length);
            for (int i = 0; i < values.length; i++)
            {
                //the originalLength acts as an offset
                data[originalLength + i] = (double) values[i];
            }
        }
        else
        {
            int numToAdd = values.length;
            if (numToAdd >= windowSize)
            {
                //The number of values is greater than the max number, so we just copy the last windowSize values into the data array
                int offset = values.length - windowSize;
                for (int i = 0; i < data.length; i++)
                {
                    data[i] = (double) values[offset + i];
                }
            }
            else
            {
                //Determine how many empty spots there are in the array, but do not allow a negative number
                int emptyCells = Math.max(0, windowSize - count);

                if (numToAdd <= emptyCells)
                {
                    //we can just put the values into the array starting at the correct index
                    int offset = count;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
                else
                {
                    //First we must shift the end values over in the data array
                    int shiftAmt = (emptyCells < numToAdd) ? (numToAdd - emptyCells) : numToAdd;
                    shiftDataArray(shiftAmt);
                    //now copy the new values onto the end
                    int offset = data.length - values.length;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
            }
            count += numToAdd;
        }
    }


    /**
     * Adds an array of {@code int} values to the data set.
     *
     * @param values the values to add.
     */
    public void addValues(int[] values)
    {
        if (values == null || values.length == 0) { return; }

        // Since they are different data types, we cannot use System.copyarray. Additionally, the squawk VM
        // has limited Array utils. So we will need to manually cast and copy each element
        if (infiniteWindow())
        {
            int originalLength = data != null ? data.length : 0;
            expandDataArray(values.length);
            for (int i = 0; i < values.length; i++)
            {
                //the originalLength acts as an offset
                data[originalLength + i] = (double) values[i];
            }
        }
        else
        {
            int numToAdd = values.length;
            if (numToAdd >= windowSize)
            {
                //The number of values is greater than the max number, so we just copy the last windowSize values into the data array
                int offset = values.length - windowSize;
                for (int i = 0; i < data.length; i++)
                {
                    data[i] = (double) values[offset + i];
                }
            }
            else
            {
                //Determine how many empty spots there are in the array, but do not allow a negative number
                int emptyCells = Math.max(0, windowSize - count);

                if (numToAdd <= emptyCells)
                {
                    //we can just put the values into the array starting at the correct index
                    int offset = count;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
                else
                {
                    //First we must shift the end values over in the data array
                    int shiftAmt = (emptyCells < numToAdd) ? (numToAdd - emptyCells) : numToAdd;
                    shiftDataArray(shiftAmt);
                    //now copy the new values onto the end
                    int offset = data.length - values.length;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
            }
            count += numToAdd;
        }
    }


    /**
     * Adds an array of {@code long} values to the data set.
     *
     * @param values the values to add.
     */
    public void addValues(long[] values)
    {
        if (values == null || values.length == 0) { return; }

        // Since they are different data types, we cannot use System.copyarray. Additionally, the squawk VM
        // has limited Array utils. So we will need to manually cast and copy each element
        if (infiniteWindow())
        {
            int originalLength = data != null ? data.length : 0;
            expandDataArray(values.length);
            for (int i = 0; i < values.length; i++)
            {
                //the originalLength acts as an offset
                data[originalLength + i] = (double) values[i];
            }
        }
        else
        {
            int numToAdd = values.length;
            if (numToAdd >= windowSize)
            {
                //The number of values is greater than the max number, so we just copy the last windowSize values into the data array
                int offset = values.length - windowSize;
                for (int i = 0; i < data.length; i++)
                {
                    data[i] = (double) values[offset + i];
                }
            }
            else
            {
                //Determine how many empty spots there are in the array, but do not allow a negative number
                int emptyCells = Math.max(0, windowSize - count);

                if (numToAdd <= emptyCells)
                {
                    //we can just put the values into the array starting at the correct index
                    int offset = count;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
                else
                {
                    //First we must shift the end values over in the data array
                    int shiftAmt = (emptyCells < numToAdd) ? (numToAdd - emptyCells) : numToAdd;
                    shiftDataArray(shiftAmt);
                    //now copy the new values onto the end
                    int offset = data.length - values.length;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
            }
            count += numToAdd;
        }
    }


    /**
     * Adds an array of {@code float} values to the data set.
     *
     * @param values the values to add.
     */
    public void addValues(float[] values)
    {
        if (values == null || values.length == 0) { return; }

        // Since they are different data types, we cannot use System.copyarray. Additionally, the squawk VM
        // has limited Array utils. So we will need to manually cast and copy each element
        if (infiniteWindow())
        {
            int originalLength = data != null ? data.length : 0;
            expandDataArray(values.length);
            for (int i = 0; i < values.length; i++)
            {
                //the originalLength acts as an offset
                data[originalLength + i] = (double) values[i];
            }
        }
        else
        {
            int numToAdd = values.length;
            if (numToAdd >= windowSize)
            {
                //The number of values is greater than the max number, so we just copy the last windowSize values into the data array
                int offset = values.length - windowSize;
                for (int i = 0; i < data.length; i++)
                {
                    data[i] = (double) values[offset + i];
                }
            }
            else
            {
                //Determine how many empty spots there are in the array, but do not allow a negative number
                int emptyCells = Math.max(0, windowSize - count);

                if (numToAdd <= emptyCells)
                {
                    //we can just put the values into the array starting at the correct index
                    int offset = count;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
                else
                {
                    //First we must shift the end values over in the data array
                    int shiftAmt = (emptyCells < numToAdd) ? (numToAdd - emptyCells) : numToAdd;
                    shiftDataArray(shiftAmt);
                    //now copy the new values onto the end
                    int offset = data.length - values.length;
                    for (int i = 0; i < values.length; i++)
                    {
                        data[offset + i] = (double) values[i];

                    }
                }
            }
            count += numToAdd;
        }
    }


    /**
     * Resets the class to prepare for a new set of data samples. This will &quot;empty&quot; the data set array (by populating it with Non a Number values) and
     * reset the data count. If the windows size was previously set, it will reset to the same windows size.
     */
    public void reset()
    {
        initDataArray();
        count = 0;
    }


    /**
     * Resets the class to prepare for a new set of data samples and also sets a new sample size. This will &quot;empty&quot; the data set array (by populating
     * it with Non a Number values) and reset the data count.
     *
     * @param windowSize new window size; to eliminate the window, set to the value {@link #INFINITE_WINDOW}.
     *
     * @throws IllegalArgumentException if the supplied windowSize is not 2 or more
     */
    public void reset(int windowSize) throws IllegalArgumentException
    {
        setWindowSize(windowSize);
        reset();
    }


    /**
     * Returns the current data set. In the event that the number of add data items is less than the target sample size, the latter portion of the array will
     * contain Not a Number ({@link Double#NaN}) values.
     *
     * @return the current data set.
     */
    public double[] getObservations()
    {
        return data;
    }


    /**
     * Returns the data set as an array of {@code byte}s, with a possible loss of magnitude and precision. If the data set was not originally populated with
     * {@code byte}s or wider data type, loss of magnitude and fractional truncation may occur. Normal Java casting rules applied. See the {@link
     * #StatsCalculator class level} documentation for more detail. Note that this method has a performance cost associated with it. In the event that the
     * number of add data items is less than the target sample size, the latter portion of the array will contain zero values.
     *
     * @return the data set as an array of {@code byte}s
     */
    public byte[] getObservationsAsBytes()
    {
        if (data == null)
        {
            return new byte[0];
        }
        else
        {

            byte[] array = new byte[data.length];
            for (int i = 0; i < data.length; i++)
            {
                array[i] = (byte) data[i];
            }
            return array;
        }
    }


    /**
     * Returns the data set as an array of {@code short}s, with a possible loss of magnitude and precision. If the data set was not originally populated with
     * {@code short}s or wider data type, loss of magnitude and fractional truncation may occur. Normal Java casting rules applied. See the {@link
     * #StatsCalculator class level} documentation for more detail. Note that this method has a performance cost associated with it. In the event that the
     * number of add data items is less than the target sample size, the latter portion of the array will contain zero values.
     *
     * @return the data set as an array of {@code short}s
     */
    public short[] getObservationsAsShorts()
    {
        if (data == null)
        {
            return new short[0];
        }
        else
        {

            short[] array = new short[data.length];
            for (int i = 0; i < data.length; i++)
            {
                array[i] = (short) data[i];
            }
            return array;
        }
    }


    /**
     * Returns the data set as an array of {@code int}s, with a possible loss of magnitude and precision. If the data set was not originally populated with
     * {@code int}s or wider data type, loss of magnitude and fractional truncation may occur. Normal Java casting rules applied. See the {@link
     * #StatsCalculator class level} documentation for more detail. Note that this method has a performance cost associated with it. In the event that the
     * number of add data items is less than the target sample size, the latter portion of the array will contain zero values.
     *
     * @return the data set as an array of {@code int}s
     */
    public int[] getObservationsAsIntegers()
    {
        if (data == null)
        {
            return new int[0];
        }
        else
        {

            int[] array = new int[data.length];
            for (int i = 0; i < data.length; i++)
            {
                array[i] = (int) data[i];
            }
            return array;
        }
    }


    /**
     * Returns the data set as an array of {@code long}s, with a possible loss of magnitude and precision. If the data set was not originally populated with
     * {@code long}s or wider data type, loss of magnitude and fractional truncation may occur. Normal Java casting rules applied. See the {@link
     * #StatsCalculator class level} documentation for more detail. Note that this method has a performance cost associated with it.
     *
     * @return the data set as an array of {@code long}s
     */
    public long[] getObservationsAsLongs()
    {
        if (data == null)
        {
            return new long[0];
        }
        else
        {

            long[] array = new long[data.length];
            for (int i = 0; i < data.length; i++)
            {
                array[i] = (long) data[i];
            }
            return array;
        }
    }


    /**
     * Returns the data set as an array of {@code float}s, with a possible loss of magnitude and magnitude. If the data set was not originally populated with
     * {@code float}s or wider data type, loss of magnitude and precision may occur. Normal Java casting rules applied. See the {@link #StatsCalculator class
     * level} documentation for more detail. Note that this method has a performance cost associated with it. In the event that the number of add data items is
     * less than the target sample size, the latter portion of the array will contain Not a Number ({@link Double#NaN}) values.
     *
     * @return the data set as an array of {@code float}s
     */
    public float[] getObservationsAsFloats()
    {
        if (data == null)
        {
            return new float[0];
        }
        else
        {

            float[] array = new float[data.length];
            for (int i = 0; i < data.length; i++)
            {
                array[i] = (float) data[i];
            }
            return array;
        }
    }


    /**
     * Convenience method to match the other {@code getDataSetAsXXX} methods, Calling this method is the semantic equivalent to calling {@link
     * #getObservations}.
     *
     * @return the data set as an array of {@code double}s
     */
    public double[] getObservationsAsDoubles()
    {
        return data;
    }


    /**
     * Returns the number of values added thus far.
     *
     * @return the number of values added thus far.
     */
    public int getObservationsAddedCount()
    {
        return infiniteWindow() ? (data != null ? data.length : 0) : count;
    }


    /**
     * Returns the number of values added thus far.
     *
     * @return the number of values added thus far.
     */
    public int getObservationsUsedCount()
    {
        return infiniteWindow() ? (data != null ? data.length : 0) : windowSize;
    }


    /**
     * Method to test if a full set of sample data has been added. That is, is the number of values added greater than or equal to the configured sample size.
     * This method cam be used as a loop conditional check so calling code can keep adding data until the target sample size has been met.
     *
     * @return true if the number of data items added is greater than or equal to the target sample size
     */
    public boolean hasFullWindow()
    {
        return count >= windowSize;
    }


    /**
     * Gets the configured window size. This is <b>not</b> necessarily the number of values added. It is the size of the window of data that must be met before
     * a standard deviation can be calculated, and the number of data items that are retained for calculating the standard deviation.
     *
     * @return the configured sample size
     */
    public int getWindowSize()
    {
        return windowSize;
    }


    /**
     * Calculates the standard deviation for the current set of data. Returns {@link Double#NaN} if the number of values added thus far is less than target
     * sample size have been added prior to calling this method. Note that this method performs the actual calculation upon each call. If the caller will need
     * to use resultant standard deviation multiple times, it should store the returned value locally rather than repeatedly calling this method in order to
     * maximize performance.
     *
     * @return the standard deviation for the data set, or {@link Double#NaN} if less than target number of values have been added
     */
    public double calcStandardDeviation()
    {
        //call the static implementation with out data set.
        return count < windowSize ? Double.NaN : calcStandardDeviation(data);
    }


    public double calcMean()
    {
        return calcMean(data);
    }


    public double calcGeometricMean()
    {
        return calcGeometricMean(data);
    }


    /**
     * Static convenience method to calculate the Standard Deviation of two or more values when they already exist in an array. Standard usage:
     * <pre>
     *     double[] myData = ... //code to populate array
     *     double result = calcStandardDeviation();
     * </pre>
     * If you need to accumulate data and then get the result, use an instance of this class, add the values, and then call {@link #calcStandardDeviation}. See
     * the {@link StatsCalculator Class Level} documentation for more details.
     *
     * @param data the set of data to calculate the Standard Deviation for
     *
     * @return the standard deviation for the supplied values, or {@link Double#NaN} if the data set is null, or has less than 2 values
     */
    public static double calcStandardDeviation(double[] data)
    {
        if (data == null)
        {
            return Double.NaN;
        }

        final int n = data.length;
        //S.D. is undefined for less than two observations
        if (n < 2)
        {
            return Double.NaN;
        }

        // S.D. is square root of sum of (values-mean) squared divided by n - 1
        double mean = calcMean(data);
        double sum = 0;
        for (int i = 0; i < n; i++)
        {
            final double v = data[i] - mean;
            sum += v * v;
        }
        // Change to ( n - 1 ) to n if you have complete data instead of a sample.
        return Math.sqrt(sum / (n - 1));
    }


    /**
     * Static convenience method to calculate the Standard Deviation of two or more values when they already exist in an array. Standard usage:
     * <pre>
     *     long[] myData = ... //code to populate array
     *     double result = calcStandardDeviation();
     * </pre>
     * If you need to accumulate data and then get the result, use an instance of this class, add the values, and then call {@link #calcStandardDeviation}. See
     * the {@link StatsCalculator Class Level} documentation for more details.
     *
     * @param data the set of data to calculate the Standard Deviation for
     *
     * @return the standard deviation for the supplied values, or {@link Double#NaN} if the data set is null, or has less than 2 values
     */
    public static double calcStandardDeviation(long[] data)
    {
        if (data == null)
        {
            return Double.NaN;
        }

        final int n = data.length;
        //S.D. is undefined for less than two observations
        if (n < 2)
        {
            return Double.NaN;
        }

        // S.D. is square root of sum of (values-mean) squared divided by n - 1
        double mean = calcMean(data);
        double sum = 0;
        for (int i = 0; i < n; i++)
        {
            final double v = data[i] - mean;
            sum += v * v;
        }
        // Change to ( n - 1 ) to n if you have complete data instead of a sample.
        return Math.sqrt(sum / (n - 1));
    }


    /**
     * Static convenience method to calculate the Standard Deviation of two or more values when they already exist in an array. Standard usage:
     * <pre>
     *     int[] myData = ... //code to populate array
     *     double result = calcStandardDeviation();
     * </pre>
     * If you need to accumulate data and then get the result, use an instance of this class, add the values, and then call {@link #calcStandardDeviation}. See
     * the {@link StatsCalculator Class Level} documentation for more details.
     *
     * @param data the set of data to calculate the Standard Deviation for
     *
     * @return the standard deviation for the supplied values, or {@link Double#NaN} if the data set is null, or has less than 2 values
     */
    public static double calcStandardDeviation(int[] data)
    {
        if (data == null)
        {
            return Double.NaN;
        }

        final int n = data.length;
        //S.D. is undefined for less than two observations
        if (n < 2)
        {
            return Double.NaN;
        }

        // S.D. is square root of sum of (values-mean) squared divided by n - 1
        double mean = calcMean(data);
        double sum = 0;
        for (int i = 0; i < n; i++)
        {
            final double v = data[i] - mean;
            sum += v * v;
        }
        // Change to ( n - 1 ) to n if you have complete data instead of a sample.
        return Math.sqrt(sum / (n - 1));
    }


    /**
     * Static convenience method to calculate the Standard Deviation of two or more values when they already exist in an array. Standard usage:
     * <pre>
     *     float[] myData = ... //code to populate array
     *     double result = calcStandardDeviation();
     * </pre>
     * If you need to accumulate data and then get the result, use an instance of this class, add the values, and then call {@link #calcStandardDeviation}. See
     * the {@link StatsCalculator Class Level} documentation for more details.
     *
     * @param data the set of data to calculate the Standard Deviation for
     *
     * @return the standard deviation for the supplied values, or {@link Double#NaN} if the data set is null, or has less than 2 values
     */
    public static double calcStandardDeviation(float[] data)
    {
        if (data == null)
        {
            return Double.NaN;
        }

        final int n = data.length;
        //S.D. is undefined for less than two observations
        if (n < 2)
        {
            return Double.NaN;
        }

        // S.D. is square root of sum of (values-mean) squared divided by n - 1
        double mean = calcMean(data);
        double sum = 0;
        for (int i = 0; i < n; i++)
        {
            final double v = data[i] - mean;
            sum += v * v;
        }
        // Change to ( n - 1 ) to n if you have complete data instead of a sample.
        return Math.sqrt(sum / (n - 1));
    }


    /**
     * Static convenience method to calculate the Standard Deviation of two or more values when they already exist in an array. Standard usage:
     * <pre>
     *     short[] myData = ... //code to populate array
     *     double result = calcStandardDeviation();
     * </pre>
     * If you need to accumulate data and then get the result, use an instance of this class, add the values, and then call {@link #calcStandardDeviation}. See
     * the {@link StatsCalculator Class Level} documentation for more details.
     *
     * @param data the set of data to calculate the Standard Deviation for
     *
     * @return the standard deviation for the supplied values, or {@link Double#NaN} if the data set is null, or has less than 2 values
     */
    public static double calcStandardDeviation(short[] data)
    {
        if (data == null)
        {
            return Double.NaN;
        }

        final int n = data.length;
        //S.D. is undefined for less than two observations
        if (n < 2)
        {
            return Double.NaN;
        }

        // S.D. is square root of sum of (values-mean) squared divided by n - 1
        double mean = calcMean(data);
        double sum = 0;
        for (int i = 0; i < n; i++)
        {
            final double v = data[i] - mean;
            sum += v * v;
        }
        // Change to ( n - 1 ) to n if you have complete data instead of a sample.
        return Math.sqrt(sum / (n - 1));
    }


    /**
     * Static convenience method to calculate the Standard Deviation of two or more values when they already exist in an array. Standard usage:
     * <pre>
     *     byte[] myData = ... //code to populate array
     *     double result = calcStandardDeviation();
     * </pre>
     * If you need to accumulate data and then get the result, use an instance of this class, add the values, and then call {@link #calcStandardDeviation}. See
     * the {@link StatsCalculator Class Level} documentation for more details.
     *
     * @param data the set of data to calculate the Standard Deviation for
     *
     * @return the standard deviation for the supplied values, or {@link Double#NaN} if the data set is null, or has less than 2 values
     */
    public static double calcStandardDeviation(byte[] data)
    {
        if (data == null)
        {
            return Double.NaN;
        }

        final int n = data.length;
        //S.D. is undefined for less than two observations
        if (n < 2)
        {
            return Double.NaN;
        }

        // S.D. is square root of sum of (values-mean) squared divided by n - 1
        double mean = calcMean(data);
        double sum = 0;
        for (int i = 0; i < n; i++)
        {
            final double v = data[i] - mean;
            sum += v * v;
        }
        // Change to ( n - 1 ) to n if you have complete data instead of a sample.
        return Math.sqrt(sum / (n - 1));
    }


    public static double calcMean(double[] data)
    {
        double sum = 0.0;
        for (int i = 0; i < data.length; i++)
        {
            sum += data[i];
        }
        return sum / data.length;
    }


    public static double calcMean(byte[] data)
    {
        double sum = 0.0;
        for (int i = 0; i < data.length; i++)
        {
            sum += data[i];
        }
        return sum / data.length;
    }


    public static double calcMean(short[] data)
    {
        double sum = 0.0;
        for (int i = 0; i < data.length; i++)
        {
            sum += data[i];
        }
        return sum / data.length;
    }


    public static double calcMean(int[] data)
    {
        double sum = 0.0;
        for (int i = 0; i < data.length; i++)
        {
            sum += data[i];
        }
        return sum / data.length;
    }


    public static double calcMean(long[] data)
    {
        double sum = 0.0;
        for (int i = 0; i < data.length; i++)
        {
            sum += data[i];
        }
        return sum / data.length;
    }


    public static double calcMean(float[] data)
    {
        double sum = 0.0;
        for (int i = 0; i < data.length; i++)
        {
            sum += data[i];
        }
        return sum / data.length;
    }


    public static double calcGeometricMean(double[] data)
    {
        double product = 1.0;
        for (int i = 0; i < data.length; i++)
        {

            double value = data[i];
            if (value < 0)
            {
                return Double.NaN;
            }
            else
            {
                product *= value;
            }
        }
        double exp = 1.0 / data.length;
        return MathUtils.pow(product, exp);
    }


    public static double calcGeometricMean(byte[] data)
    {
        double product = 1.0;
        for (int i = 0; i < data.length; i++)
        {

            double value = data[i];
            if (value < 0)
            {
                return Double.NaN;
            }
            else if (value == 0.0)
            {
                return 0.0;
            }
            else
            {
                product *= value;
            }
        }
        double exp = 1.0 / data.length;
        return MathUtils.pow(product, exp);
    }


    public static double calcGeometricMean(short[] data)
    {
        double product = 1.0;
        for (int i = 0; i < data.length; i++)
        {

            double value = data[i];
            if (value < 0)
            {
                return Double.NaN;
            }
            else if (value == 0.0)
            {
                return 0.0;
            }
            else
            {
                product *= value;
            }
        }
        double exp = 1.0 / data.length;
        return MathUtils.pow(product, exp);
    }


    public static double calcGeometricMean(int[] data)
    {
        double product = 1.0;
        for (int i = 0; i < data.length; i++)
        {

            double value = data[i];
            if (value < 0)
            {
                return Double.NaN;
            }
            else if (value == 0.0)
            {
                return 0.0;
            }
            else
            {
                product *= value;
            }
        }
        double exp = 1.0 / data.length;
        return MathUtils.pow(product, exp);
    }


    public static double calcGeometricMean(long[] data)
    {
        double product = 1.0;
        for (int i = 0; i < data.length; i++)
        {

            double value = data[i];
            if (value < 0)
            {
                return Double.NaN;
            }
            else if (value == 0.0)
            {
                return 0.0;
            }
            else
            {
                product *= value;
            }
        }
        double exp = 1.0 / data.length;
        return MathUtils.pow(product, exp);
    }


    public static double calcGeometricMean(float[] data)
    {
        double product = 1.0;
        for (int i = 0; i < data.length; i++)
        {

            double value = data[i];
            if (value < 0)
            {
                return Double.NaN;
            }
            else if (value == 0.0)
            {
                return 0.0;
            }
            else
            {
                product *= value;
            }
        }
        double exp = 1.0 / data.length;
        return MathUtils.pow(product, exp);
    }
}
