package frc3838.robot.utils.math;


import com.sun.squawk.util.MathUtils;



public final class MathAids
{
    private MathAids(){}


    /**
     * Converts a decimal to a percentage String (including the '%' sign) with no decimal places and no rounding.
     *
     * @param value the value to convert to a percentage
     *
     * @return the value as a percentage String
     */
    public static String toPercentage(double value)
    {
        return toPercentage(value, 0, false);
    }


    /**
     * Converts a decimal number to a percentage String (including the '%' sign).
     *
     * @param value     the value to convert to a percentage
     * @param precision the number of decimal places to have in the percentage value; must be between 0 and 5 inclusive
     * @param round     if true, the value will be rounded up, otherwise irt will be floored
     *
     * @return the value as a percentage String
     */
    public static String toPercentage(double value, int precision, boolean round)
    {

        double roundFactor = round ? 0.5d : 0.0;
        if (precision <= 0)
        {
            return Long.toString((long) Math.floor(value * 100 + roundFactor)) + '%';
        }
        else
        {
            //technically we should make sure that the value * the shiftFactor does not exceed
            // Double.MAX_VALUE.  But that is an unlikely use case, so we favor performance over safety
            precision = Math.min(5, precision);
            double shiftFactor = MathUtils.pow(10, precision);
            long shifted = (long) Math.floor(value * 100 * shiftFactor + roundFactor);
            double result = shifted / (shiftFactor);
            return Double.toString(result) + '%';
        }
    }
}
