package frc3838.robot;

import edu.wpi.first.wpilibj.DriverStationLCD;
import edu.wpi.first.wpilibj.Joystick;
import frc3838.robot.components.LimitableJoystick;
import frc3838.robot.utils.LogLevel;


/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 * @noinspection UtilityClassWithoutPrivateConstructor
 */
public class RobotMap
{


    private static final int Unassigned = -99;

    public static boolean inAutonomousMode = false;


    //private constructor to prevent instantiation as all access should be to static members
    private RobotMap() { }

    public static final LogLevel LOG_LEVEL = LogLevel.TRACE;
    public static final boolean IN_DEBUG_MODE = true;


    public static final class AnalogIO
    {
        public static final int GYRO_TEMP_ADDR = 1;
        public static final int GYRO_RATE_ADDR = 2;
        public static final int PROXIMITY_CHANNEL = 4;
    }

    public static final class Constants
    {
        public static final int TURRET_MOTOR_PWM_START = 128;
        public static final int TURRET_PAN_CHANGE_AMOUNT = 1;
        public static final int TURRET_PAN_MAX_VALUE = 255;
        public static final int TURRET_PAN_MIN_VALUE = 1;
    }

    public static final class DigitalIO
    {
        public static final int ACCELEROMETER_ADDR = Unassigned;
        public static final int TURRET_ENCODER_A = 1;
        public static final int TURRET_ENCODER_B = 2;
        public static final int RIGHT_WHEEL_ENCODER_A = 5;
        public static final int RIGHT_WHEEL_ENCODER_B = 6;
        public static final int LEFT_WHEEL_ENCODER_A = 7;
        public static final int LEFT_WHEEL_ENCODER_B = 8;
        public static final int THROW_MOTOR_ENCODER = 9;

        public static final int TURRET_LIMIT_CCW = 11;
        public static final int TURRET_LIMIT_CW = 12;
        //Compressor constructors:
        //     Compressor(int pressureSwitchSlot, int pressureSwitchChannel, int compressorRelaySlot, int compressorRelayChannel)
        //     Compressor(int pressureSwitchChannel, int compressorRelayChannel)
        // compressor relay specified in relays class
        public static final int COMPRESSOR_PRESSURE_SWITCH_CHANNEL = 14;
    }

    public static final class I2CBus
    {
        public final static int COMPASS_SLOT = 1;
    }

    public static final class PWMChannels
    {
        public static final int RIGHT_WHEELS_CHANNEL = 1;
        public static final int THROW_MOTOR_CHANNEL = 3;
        public static final int SHIFTER_CHANNEL = 5;
        public static final int TURRET_MOTOR_CHANNEL = 2;
        public static final int LEFT_WHEELS_CHANNEL = 10;
    }

    public static final class Relays
    {
        public static final int LED_RING = 1;
        public static final int BALL_RAMP_MOTOR = 2;
        //Compressor constructors:
        //     Compressor(int pressureSwitchSlot, int pressureSwitchChannel, int compressorRelaySlot, int compressorRelayChannel)
        //     Compressor(int pressureSwitchChannel, int compressorRelayChannel)
        // compressor pressure switch is configured in DigitalIO class
        public static final int COMPRESSOR_RELAY_CHANNEL = 3;
        //public static final int BRIDGE_AIR_CYLINDER = -99; //TODO: get real value
    }

    public static final class Solenoids
    {
        public static final int FIRINGPIN_DOWN_AIR_CYLINDER_ADDR = 2;  // Normally On
        public static final int FIRINGPIN_UP_AIR_CYLINDER_ADDR = 1; // Normally Off
        public static final int BRIDGE_DOWN_AIR_CYLINDER_ADDR = 4;// Normally Off
        public static final int BRIDGE_UP_AIR_CYLINDER_ADDR = 3; // Normally On
    }

    public static final class ControlStationMap
    {
        private static final int JOYSTICK_LEFT = 1;
        private static final int JOYSTICK_RIGHT = 2;
        public static final LimitableJoystick DRIVER_JOYSTICK = new LimitableJoystick(JOYSTICK_LEFT);
        public static final Joystick GUNNER_JOYSTICK = new Joystick(JOYSTICK_RIGHT);

        //DriverStationLCD line assignments
        public static final DriverStationLCD.Line SHOOTER_STATUS_LCD_LINE = DriverStationLCD.Line.kMain6;
//        public static final DriverStationLCD.Line LINE_TWO = DriverStationLCD.Line.kUser2;
//        public static final DriverStationLCD.Line LINE_THREE = DriverStationLCD.Line.kUser3;
//        public static final DriverStationLCD.Line SHOOTER_FOUR = DriverStationLCD.Line.kUser4;
//        public static final DriverStationLCD.Line SHOOTER_FIVE = DriverStationLCD.Line.kUser5;
//        public static final DriverStationLCD.Line SHOOTER_SIX = DriverStationLCD.Line.kUser6;

        public static final class DriverJoystickButtons
        {
            // (See diagram below)
            // Joystick buttons:
            //  1 Trigger
            //  2 Bottom Thumb
            public static final int SHIFT_TO_LOW_GEAR_BTN_NUM = 2;
            //  3 Top Thumb
            public static final int SHIFT_TO_HIGH_GEAR_BTN_NUM = 3;
            //  4 Left Thumb
            public static final int LIMITED_SPEED_ON_BTN_NUM = 4;
            //  5 Right Thumb
            public static final int LIMITED_SPEED_OFF_BTN_NUM = 5;
            //  6 Left Group Front
            public static final int BRIDGE_DOWN_BTN_NUM = 6;
            //  7 Left Group Back
            //  8 Bottom Group Left
            //  9 Bottom Group Right
            // 10 Right Group Back
            // 11 Right Group Front
        }

        public static final class GunnerJoystickButtons
        {
            // (See diagram below)
            // Joystick buttons:
            //  1 Trigger
            public static final int TARGET_AND_SHOOT_BTN_NUM = 1;
            //  2 Bottom Thumb      Button will need to be held down to run motor slowly in reverse to clear a jam
            public static final int THROW_MOTOR_REVERSE_BTN_NUM = 2;
            //  3 Top Thumb         Button will need to be held down to allow Joystick Y to act as speed increase or decrease
            public static final int THROW_MOTOR_SPEED_CHANGE_SHIFT_BTN_NUM = 3;
            //  4 Left Thumb
            public static final int THROW_MOTOR_STOP_BTN_NUM = 4;
            //  5 Right Thumb
            public static final int THROW_MOTOR_START_BTN_NUM = 5;
            //  6 Left Group Front
            //  7 Left Group Back
            //  8 Bottom Group Left
            public static final int DECREASE_THROW_MOTOR_SPEED_BTN_NUM = 8;
            //  9 Bottom Group Right
            public static final int INCREASE_THROW_MOTOR_SPEED_BTN_NUM = 9;
            // 10 Right Group Back
            public static final int LED_ON_OFF_BTN_NUM = 10;
            // 11 Right Group Front


            public static final int CYCLE_HEIGHT_BTN_NUM = Unassigned;
            public static final int DECR_DIST_BTN_NUM = Unassigned;
            public static final int INCR_DIST_BTN_NUM = Unassigned;


            /*

                Shift to change motor speed = 3
                Stop motor = 4
                Start Motor = 5
                Reverse Motor = 2

             */



        }

            //         JOYSTICK BUTTON LAYOUT
            // **************************************
            // *              (Trigger)             *
            // *          .................         *
            // *          :               :         *
            // * +---+    :     +---+     :   +---+ *
            // * | 6 |    . +-+ | 3 | +-+ :   + 11| *
            // * +---+    : |4| +---+ |5| :   +---+ *
            // *          : +-+ +---+ +-+ :         *
            // * +---+    :     | 2 |     :   +---+ *
            // * | 7 |    :     +---+     :   | 10| *
            // * +---+    :...............:   +---+ *
            // * Left          Stick          Right *
            // * Group      +---+  +---+      Group *
            // *            | 8 |  | 9 |            *
            // *            +---+  +---+            *
            // *            Bottom Group            *
            // **************************************

    }


}
