package frc3838.robot.components;

import edu.wpi.first.wpilibj.Joystick;

public class LimitableJoystick extends Joystick
{
    private boolean inLimitedMode = false;

    private double xLimit = 0.5;
    private double yLimit = 0.5;
    
    public LimitableJoystick(int port)
    {
        super(port);
    }

    public LimitableJoystick(int port, int numAxisTypes, int numButtonTypes)
    {
        super(port, numAxisTypes, numButtonTypes);
    }

    public double getY(Hand hand)
    {
        return limit(super.getY(hand), yLimit); 
    }

    public double getX(Hand hand)
    {
        return limit(super.getX(hand), xLimit);
    }

    public double getAxis(AxisType axis)
    {
        return super.getAxis(axis);
    }

    private double limit(double value, double limit)
    {
        if (inLimitedMode)
        {
            double abs = Math.abs(value);
            if (abs > limit)
            {
                if (value <= 0)
                {
                    return limit;
                }
                else
                {
                    return -limit;
                }
            }
        }
        return value;
    }

    public boolean isInLimitMode()
    {
        return inLimitedMode;
    }

    public void setInLimitMode(boolean inLimitedMode)
    {
        this.inLimitedMode = inLimitedMode;
    }

    public boolean toggleLimitMode()
    {
        inLimitedMode = !inLimitedMode;
        return inLimitedMode;
    }

    public void turnOnLimitMode()
    {
        inLimitedMode = true;
    }

    public void turnOffLimitMode()
    {
        inLimitedMode = true;
    }

    public double getXLimit()
    {
        return xLimit;
    }

    public void setXLimit(double xLimit)
    {
        if (xLimit <= 0 || xLimit >= 1)
        {
            throw  new IllegalArgumentException("X Limit must be between 0 and 1");
        }
        this.xLimit = xLimit;
    }

    public double getYLimit()
    {
        return yLimit;
    }

    public void setYLimit(double yLimit)
    {
        if (xLimit <= 0 || xLimit >= 1)
        {
            throw  new IllegalArgumentException("Y Limit must be between 0 and 1");
        }
        this.yLimit = yLimit;
    }
}
