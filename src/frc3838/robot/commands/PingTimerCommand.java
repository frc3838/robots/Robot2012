package frc3838.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.robot.utils.LOG;



/** @author student18 */
public class PingTimerCommand extends CommandBase
{
    private static long maxTime = 0;
    private static long minTime = Long.MAX_VALUE;
    private static long totalTime = 0;
    private static int count = 0;

    private long lastExecutionTime = System.currentTimeMillis();


    public PingTimerCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(timingSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        LOG.debug("PingTimerCommand initialize() called");
        recordMinTime();
        recordMaxTime();
    }


    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        count++;
        long currentTime = System.currentTimeMillis();
        //We need to ignore the first run through, in order to have good values for min and max
        //if we init them to arbitrary values, it will skew the results.
        if (count > 1)
        {
            long difference = currentTime - lastExecutionTime;
            //update SmartDashboard;
            recordTimeDifference(difference);

            //test max;
            if (difference > maxTime)
            {
                maxTime = difference;
                recordMaxTime();
            }

            //test min
            if (difference < minTime)
            {
                minTime = difference;
                recordMinTime();
            }


            totalTime += difference;
            double avg = totalTime / count;
            recordAverageTime(avg);
        }
        //set the lastExecution time for next loop run
        lastExecutionTime = System.currentTimeMillis();
    }


    private void recordMaxTime()
    {
        // If we use the putDouble method, the smartDashboard displays in scientific notation with
        // the Exponent truncated off because the field length is too long. So we use putString
        SmartDashboard.putString("Max time", Long.toString(maxTime) + "ms");
    }


    private void recordMinTime()
    {
        // If we use the putDouble method, the smartDashboard displays in scientific notation with
        // the Exponent truncated off because the field length is too long. So we use putString
        SmartDashboard.putString("Min time", Long.toString(minTime) + "ms");
    }


    private void recordTimeDifference(long difference)
    {
        // If we use the putDouble method, the smartDashboard displays in scientific notation with
        // the Exponent truncated off because the field length is too long. So we use putString
        SmartDashboard.putString("Time diff", Long.toString(difference) + "ms");
    }


    private void recordAverageTime(double average)
    {
        // If we use the putDouble method, the smartDashboard displays in scientific notation with
        // the Exponent truncated off because the field length is too long. So we use putString
        SmartDashboard.putString("Avg time", Double.toString(average) + "ms");
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }


    // Called once after isFinished returns true
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
