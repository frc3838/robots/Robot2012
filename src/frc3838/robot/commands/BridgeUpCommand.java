/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.commands;

import frc3838.robot.utils.LOG;

public class BridgeUpCommand extends CommandBase
{
    public BridgeUpCommand()
    {
        requires (bridgeArmSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        LOG.trace("calling BridgeArmSubsystem.moveBridgeArmUp");
        bridgeArmSubsystem.moveBridgeArmUp();
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
