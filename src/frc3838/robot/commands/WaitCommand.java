package frc3838.robot.commands;


public class WaitCommand extends CommandBase
{
    private long startTime;
    private long delay;
    private boolean finished = false;


    /**
     * Creates a wait command to wait the specified number of seconds.
     * @param seconds seconds to wait
     */
    public WaitCommand(int seconds)
    {
        delay = seconds * 1000;
    }


    /**
     * Creates a wait command to wait the specified number of milliseconds.
     * @param milliseconds milliseconds to wait.
     */
    public WaitCommand(long milliseconds)
    {
        delay = milliseconds;
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        startTimer();
    }


    private void startTimer()
    {
        finished = false;
        startTime = System.currentTimeMillis();
    }


    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        long diff = System.currentTimeMillis() - startTime;
        finished = diff >= delay;
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return finished;
    }


    // Called once after isFinished returns true
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
