package frc3838.robot.commands.shooting;

import frc3838.robot.commands.CommandBase;

public class IncreaseThrowMotorSpeedCommand extends CommandBase
{

    public IncreaseThrowMotorSpeedCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(throwerSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        throwerSubsystem.increaseThrowMotorSpeedSetting(10);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
