package frc3838.robot.commands.shooting;

import edu.wpi.first.wpilibj.Joystick;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.commands.CommandBase;
import frc3838.robot.subsystems.TurretSubsystem;
import frc3838.robot.utils.LOG;

public class OperateTurretCommand extends CommandBase
{
    private static final Joystick joystick = RobotMap.ControlStationMap.GUNNER_JOYSTICK;
    private static final double threshold = 0.1;

    public OperateTurretCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(turretSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        double x = joystick.getX();
        if (x < -threshold )
        {
            if (TurretSubsystem.DEBUG_TURRET && Setup.isTurretSubsystemEnabled())
            {
                LOG.trace("calling turretSubsystem.panTurretLeft()");
            }

             turretSubsystem.panTurretLeft();
        }
        else if (x > threshold)
        {
            if (TurretSubsystem.DEBUG_TURRET && Setup.isTurretSubsystemEnabled())
            {
                LOG.trace("calling turretSubsystem.panTurretRight()");
            }
            turretSubsystem.panTurretRight();
        }
        else
        {
            if (TurretSubsystem.DEBUG_TURRET && Setup.isTurretSubsystemEnabled())
            {
                LOG.trace("Joystick X value below dead zone threshold. Not panning.");
            }
        }

        if (TurretSubsystem.DEBUG_TURRET && Setup.isTurretSubsystemEnabled())
        {
            LOG.trace("CCW Limit: " +  turretSubsystem.getTurretCCWLimit() + "  CW Limit: " + turretSubsystem.getTurretCWLimit());
        }


        turretSubsystem.updateSmartDashboard();


    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
