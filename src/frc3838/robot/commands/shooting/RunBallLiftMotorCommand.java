package frc3838.robot.commands.shooting;

import edu.wpi.first.wpilibj.Joystick;
import frc3838.robot.RobotMap;
import frc3838.robot.commands.CommandBase;

public class RunBallLiftMotorCommand extends CommandBase
{
    private static Joystick joystick = RobotMap.ControlStationMap.GUNNER_JOYSTICK;

    public RunBallLiftMotorCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(ballLiftSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        if (!RobotMap.inAutonomousMode)
        {
            //The values coming in are opposite what is expected, so we negate the value
            double z = -joystick.getZ();
            if (z >= 0.9)
            {
                ballLiftSubsystem.ballLiftMotorForward();
            }
            else if (z <= -0.9)
            {
                ballLiftSubsystem.ballLiftMotorReverse();
            }
            else
            {
                ballLiftSubsystem.ballLiftMotorOff();
            }
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
