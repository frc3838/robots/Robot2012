package frc3838.robot.commands.shooting;

import frc3838.robot.commands.CommandBase;
import frc3838.robot.utils.LOG;

public class ReverseThrowMotorCommand extends CommandBase
{

    public ReverseThrowMotorCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(throwerSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
//        throwerSubsystem.throwMotorStop();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        LOG.info("*** REVERSE THROW MOTOR IS NOT IMPLEMENTED YET ***");
//        throwerSubsystem.throwMotorReverse();
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }

    // Called once after isFinished returns true
    protected void end()
    {
//        throwerSubsystem.throwMotorStop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
